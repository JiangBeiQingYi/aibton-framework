/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api;

import java.util.List;

import org.springframework.util.CollectionUtils;

import com.aibton.framework.api.Interceptor.IBaseApiInterceptor;
import com.aibton.framework.api.data.EngineContext;
import com.aibton.framework.api.handler.IBaseApiHandler;
import com.aibton.framework.data.BaseResponse;

/**
 * API执行引擎具体实现
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/20 15:41 huzhihui Exp $$
 */
public class ApiEngineImpl implements IApiEngine {

    @Override
    public BaseResponse run(EngineContext engineContext) {
        //执行基础的拦截器和处理器
        doBaseInterceptorAndHandel(engineContext);
        //执行用户自定义的拦截器和处理器
        doUserApiInterceptorAndHandel(engineContext);
        //主要方法excute方法执行
        BaseResponse baseResponse = engineContext.getAbstractBaseApi()
            .doExcute(engineContext.getBaseRequest(), engineContext.getBaseResponse());
        engineContext.setBaseResponse(baseResponse);
        //执行后置处理器和拦截器
        doBaseAfterInterceptorAndHandel(engineContext);
        //执行用户自定义的后置拦截器和处理器
        doUserAfterApiInterceptorAndHandel(engineContext);
        //返回值
        return baseResponse;
    }

    /**
     * 执行基础服务
     * @param engineContext
     */
    private void doBaseInterceptorAndHandel(EngineContext engineContext) {
        List<IBaseApiHandler> iBaseApiHandlers = engineContext.getApiHandels();
        if (!CollectionUtils.isEmpty(iBaseApiHandlers)) {
            for (IBaseApiHandler iBaseApiHandler : iBaseApiHandlers) {
                iBaseApiHandler.doHandel(engineContext);
            }
        }
        List<IBaseApiInterceptor> iBaseApiInterceptors = engineContext.getApiInterceptors();
        if (!CollectionUtils.isEmpty(iBaseApiInterceptors)) {
            for (IBaseApiInterceptor iBaseApiInterceptor : iBaseApiInterceptors) {
                iBaseApiInterceptor.doIntercetpor(engineContext.getBaseRequest(),
                    engineContext.getBaseResponse());
            }
        }
    }

    /**
     * 执行后置的拦截器和处理器
     * @param engineContext
     */
    private void doBaseAfterInterceptorAndHandel(EngineContext engineContext) {
        List<IBaseApiHandler> iBaseApiHandlers = engineContext.getAfterApiHandels();
        if (!CollectionUtils.isEmpty(iBaseApiHandlers)) {
            for (IBaseApiHandler iBaseApiHandler : iBaseApiHandlers) {
                iBaseApiHandler.doHandel(engineContext);
            }
        }
        List<IBaseApiInterceptor> iBaseApiInterceptors = engineContext.getApiInterceptors();
        if (!CollectionUtils.isEmpty(iBaseApiInterceptors)) {
            for (IBaseApiInterceptor iBaseApiInterceptor : iBaseApiInterceptors) {
                iBaseApiInterceptor.doIntercetpor(engineContext.getBaseRequest(),
                    engineContext.getBaseResponse());
            }
        }
    }

    /**
     * 执行用户自定义的前置拦截器和处理器
     */
    private void doUserApiInterceptorAndHandel(EngineContext engineContext) {
        //用户自定义前置拦截器
        List<IBaseApiInterceptor> iBaseApiInterceptors = engineContext.getAbstractBaseApi()
            .getUserIBaseApiInterceptors();
        if (!CollectionUtils.isEmpty(iBaseApiInterceptors)) {
            for (IBaseApiInterceptor iBaseApiInterceptor : iBaseApiInterceptors) {
                iBaseApiInterceptor.doIntercetpor(engineContext.getBaseRequest(),
                    engineContext.getBaseResponse());
            }
        }
        //用户自定义前置处理器
        List<IBaseApiHandler> iBaseApiHandlers = engineContext.getAbstractBaseApi()
            .getUserIBaseApiHandels();
        if (!CollectionUtils.isEmpty(iBaseApiHandlers)) {
            for (IBaseApiHandler iBaseApiHandler : iBaseApiHandlers) {
                iBaseApiHandler.doHandel(engineContext);
            }
        }

    }

    /**
     * 执行用户自定义的后置拦截器和处理器
     */
    private void doUserAfterApiInterceptorAndHandel(EngineContext engineContext) {
        //用户自定义后置拦截器
        List<IBaseApiInterceptor> iBaseApiInterceptors = engineContext.getAbstractBaseApi()
            .getUserAfterIBaseApiInterceptors();
        if (!CollectionUtils.isEmpty(iBaseApiInterceptors)) {
            for (IBaseApiInterceptor iBaseApiInterceptor : iBaseApiInterceptors) {
                iBaseApiInterceptor.doIntercetpor(engineContext.getBaseRequest(),
                    engineContext.getBaseResponse());
            }
        }
        //用户自定义前置处理器
        List<IBaseApiHandler> iBaseApiHandlers = engineContext.getAbstractBaseApi()
            .getUserAfterIBaseApiHandels();
        if (!CollectionUtils.isEmpty(iBaseApiHandlers)) {
            for (IBaseApiHandler iBaseApiHandler : iBaseApiHandlers) {
                iBaseApiHandler.doHandel(engineContext);
            }
        }

    }
}
