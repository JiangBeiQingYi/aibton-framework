/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aibton.framework.api.config.AibtonApiConstantKey;
import com.aibton.framework.api.data.EngineContext;
import com.aibton.framework.api.utils.AibtonApiEngineUtils;
import com.aibton.framework.data.BaseRequest;
import com.aibton.framework.util.AssertUtils;
import com.aibton.framework.util.JackSonUtils;

/**
 * 请求参数处理器
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/21 9:26 huzhihui Exp $$
 */
public class RequestDataHandler implements IBaseApiHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestDataHandler.class);

    @Override
    public void doHandel(EngineContext engineContext) {
        AssertUtils.isNotEmpty(LOGGER, engineContext.getRequestData(),
            AibtonApiConstantKey.REQUEST_DATA_NULL);
        engineContext.setBaseRequest((BaseRequest) JackSonUtils.jsonStrToObject(
            engineContext.getRequestData(),
            AibtonApiEngineUtils.getGenericRestrictions(engineContext.getAbstractBaseApi(), 0)));
    }

}
