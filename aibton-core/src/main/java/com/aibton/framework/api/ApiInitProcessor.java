/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api;

import com.aibton.framework.api.annotation.Api;
import com.aibton.framework.api.config.AibtonApiConstantKey;
import com.aibton.framework.api.ioc.AibtonApiIoc;
import com.aibton.framework.util.AssertUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.annotation.Annotation;

/**
 * API注解的Bean的初始化
 * 默认使用bean创建后
 *
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/21 14:54 huzhihui Exp $$
 */
public class ApiInitProcessor implements BeanPostProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiInitProcessor.class);

    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        return o;
    }

    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        if (o instanceof AbstractBaseApi) {
            Class<?> objectClass = o.getClass();
            Annotation[] arrayAno = objectClass.getAnnotations();
            for (Annotation annotation : arrayAno) {
                if ("Api".equals(annotation.annotationType().getSimpleName())) {
                    Api api = (Api) annotation;
                    AssertUtils.isNotEmpty(LOGGER, api.value(),
                            AibtonApiConstantKey.API_INIT_ERROR);
                    AibtonApiIoc.addApiBean(api.value(), (AbstractBaseApi) o);
                }
            }
        }
        return o;
    }
}
