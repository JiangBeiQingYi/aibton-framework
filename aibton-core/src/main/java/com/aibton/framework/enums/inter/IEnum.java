/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.enums.inter;

/**
 * 系统配置枚举基类
 * 使用方式：枚举类需要实现IEnum接口，按照EnumConfigSimple写法
 *
 * @author huzhihui
 * @version v 0.1 2017/5/9 22:23 huzhihui Exp $$
 */
public interface IEnum<C, V, G> {

    /**
     * 获取code值
     *
     * @return code值
     */
    C getCode();

    /**
     * 获取value值
     *
     * @return value值
     */
    V getValue();

    /**
     * 获取分组
     *
     * @return 分组
     */
    G getGroup();

    /**
     * 获得枚举编码
     * @param value value
     * @param group group
     * @return 枚举编码
     */
    //C getCode(V value, G group);

    /**
     * 获得枚举值
     * @param code  code
     * @param group group
     * @return 枚举值
     */
    //V getValue(C code, G group);

    /**
     * 获取枚举名称
     *
     * @return 获取枚举名称
     */
    default String getName() {
        return ((Enum) this).name();
    }
}
