/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.data;

/**
 * 基础分页请求bean
 * @author huzhihui
 * @version v 0.1 2017/5/11 22:00 huzhihui Exp $$
 */
public class BasePageRequest extends BaseRequest {

    /**
     * 当前页
     */
    private Integer currentPage = 1;

    /**
     * 一页大小
     */
    private Integer pageSize    = 10;

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
