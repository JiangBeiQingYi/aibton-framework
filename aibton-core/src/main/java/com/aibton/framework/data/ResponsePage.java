/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.data;

import com.aibton.framework.config.AibtonConstantKey;

/**
 * 分页请求返回
 * @author huzhihui
 * @version v 0.1 2017/4/9 18:23 huzhihui Exp $$
 */
public class ResponsePage<T> extends BaseResponse {
    /**
     * 是否请求成功：true：false
     */
    private boolean success     = false;

    /**
     * 返回code
     */
    private String  code        = AibtonConstantKey.RESPONSE_000000;

    /**
     * 当前页
     */
    private Integer currentPage = 1;

    /**
     * 一页大小
     */
    private Integer pageSize    = 10;

    /**
     * 总行数
     */
    private Integer totalLine   = 0;

    /**
     * 总页数
     */
    private Integer totalPage;

    /**
     * 返回的bean
     */
    private T       msg;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public T getMsg() {
        return msg;
    }

    public void setMsg(T msg) {
        this.msg = msg;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getTotalLine() {
        return totalLine;
    }

    public void setTotalLine(Integer totalLine) {
        this.totalLine = totalLine;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    /**
     * 分页返回参数
     * @param success   请求成功与否状态
     * @param currentPage   当前页
     * @param pageSize  一页大小
     * @param totalLine 总行数
     * @param totalPage 总页数
     * @param msg   列表数据
     * @param <T>   泛型类
     * @return  分页对象
     */
    public static <T> ResponsePage<T> getData(boolean success, Integer currentPage,
                                              Integer pageSize, Integer totalLine,
                                              Integer totalPage, T msg) {
        ResponsePage<T> responsePage = new ResponsePage<T>();
        responsePage.setSuccess(success);
        responsePage.setCurrentPage(currentPage);
        responsePage.setPageSize(pageSize);
        responsePage.setTotalLine(totalLine);
        responsePage.setTotalPage(totalPage);
        responsePage.setMsg(msg);
        return responsePage;
    }
}
