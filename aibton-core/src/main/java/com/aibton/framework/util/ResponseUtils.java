/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.util;

import com.aibton.framework.data.ResponseNormal;
import com.aibton.framework.data.ResponsePage;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

/**
 * 返回页面工具类
 *
 * @author huzhihui
 * @version v 0.1 2017/4/10 22:15 huzhihui Exp $$
 */
public class ResponseUtils {

    /**
     * 普通返回
     *
     * @param success 请求成功与否状态
     * @param msg     数据
     * @param <T>     泛型类
     * @return ResponseNormal
     */
    public static <T> ResponseNormal<T> getData(boolean success, T msg) {
        return ResponseNormal.getData(success, msg);
    }

    /**
     * 其他返回
     *
     * @param success 请求成功与否状态
     * @param code    编码
     * @param msg     数据
     * @param <T>     泛型类
     * @return ResponseNormal
     */
    public static <T> ResponseNormal<T> getOtherData(boolean success, String code, T msg) {
        return ResponseNormal.getData(success, code, msg);
    }

    /**
     * 分页返回参数
     *
     * @param success     请求成功与否状态
     * @param currentPage 当前页
     * @param pageSize    一页大小
     * @param totalLine   总行数
     * @param totalPage   总页数
     * @param msg         列表数据
     * @param <T>         泛型类
     * @return ResponsePage
     */
    public static <T> ResponsePage<T> getPageData(boolean success, Integer currentPage,
                                                  Integer pageSize, Integer totalLine,
                                                  Integer totalPage, T msg) {
        return ResponsePage.getData(success, currentPage, pageSize, totalLine, totalPage, msg);
    }

    /**
     * 分页返回参数
     *
     * @param success  请求成功与否状态
     * @param pageInfo mybatis分页插件对象
     * @param <T>      泛型类
     * @return
     */
    public static <T> ResponsePage<T> getPageData(boolean success, PageInfo pageInfo) {
        return ResponsePage.getData(success, pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), (T) pageInfo.getList());
    }

    /**
     * 分页返回参数
     *
     * @param success 请求成功与否状态
     * @param page    mybatis分页插件对象
     * @param <T>     泛型类
     * @return
     */
    public static <T> ResponsePage<T> getPageData(boolean success, Page page) {
        return ResponsePage.getData(success, page.getPageNum(), page.getPageSize(), (int) page.getTotal(), page.getPages(), (T) page.getResult());
    }
}
