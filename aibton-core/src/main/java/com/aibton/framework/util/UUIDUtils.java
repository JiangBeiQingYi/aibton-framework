/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.util;

import java.util.UUID;

/**
 * UUID工具类
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/20 9:15 huzhihui Exp $$
 */
public class UUIDUtils {

    /**
     * 获取普通UUID
     * @return  如fsfs-fdsfs-fdsf
     */
    public static String getUUIDStr() {
        return UUID.randomUUID().toString();
    }

    /**
     * 获取去除了-的UUID串
     * @return  如fsdfsfsfsf
     */
    public static String getUUIDShort() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
